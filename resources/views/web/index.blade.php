@extends('web.layout.app')
@section('content')
    <section id="bannerwrepper">
        {{--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">--}}
            {{--<ol class="carousel-indicators">--}}
                {{--@foreach($slider as $item)--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class={{ $loop->first ? ' active' : '' }}></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>--}}
                    {{--@endforeach--}}
            {{--</ol>--}}
            {{--<div class="carousel-inner">--}}
                {{--<div class="carousel-item active">--}}
                    {{--<img style="width: 100%;" src="{{asset('web-assets/images/banner1.jpg')}}" alt="" />--}}
                    {{--<div class="captionwrepper">--}}
                        {{--<h1>professional labour<br> at your service</h1>--}}
                        {{--<a href="#." class="btn btn-info">Contact us Now</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="carousel-item">--}}
                    {{--<img style="width: 100%;" src="{{asset('web-assets/images/banner2.jpg')}}" alt="" />--}}
                    {{--<div class="captionwrepper">--}}
                        {{--<h1>professional labour<br> at your service</h1>--}}
                        {{--<a href="#." class="btn btn-info">Contact us Now</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="carousel-item">--}}
                    {{--<img style="width: 100%;" src="{{asset('web-assets/images/Banners2.jpg')}}" alt="" />--}}
                    {{--<div class="captionwrepper">--}}
                        {{--<h1>professional labour<br> at your service</h1>--}}
                        {{--<a href="#." class="btn btn-info">Contact us Now</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="carousel-inner" role="listbox">--}}
                {{--@foreach($slider as $item)--}}
                    {{--<div class="item {{ $loop->first ? ' active' : '' }}" >--}}
                        {{--<img style="height: 1200px" src="{{asset('assets/uploads/slider-pics/'.$item->img)}}">--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
            {{--<!-- Controls -->--}}
            {{--<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">--}}
                {{--<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>--}}
                {{--<span class="sr-only">Previous</span>--}}
            {{--</a>--}}
            {{--<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">--}}
                {{--<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>--}}
                {{--<span class="sr-only">Next</span>--}}
            {{--</a>--}}
        {{--</div>--}}


        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                @foreach($slider as $item)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>

            <div class="carousel-inner" role="listbox">
                @foreach($slider as $item)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <img style="width: 100%;"  src="{{asset('assets/uploads/slider-pics/'.$item->img)}}" alt="" />
                        {{--<div class="carousel-caption d-none d-md-block">--}}
                            <div class="captionwrepper">
                            {{--<h1>{{$item->first_heading}}<br> {{$item->second_heading}}</h1>--}}
                            {{--<a href="#." class="btn btn-info">Contact us Now</a>--}}
                            </div>
                        {{--</div>--}}
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <section id="aboutwrepper">
        <div class="container">
            <div class="row">
                <div style="text-align: left" class="col-md-6">
                    <h1>ABOUT US</h1>
                    <p>Labourhazir help you to find right and experienced Profesionals for your requirements.</p>
                    <p>We provide you platform to list down your requirements and based upon same we provide you the best solutions. We offer you the best Premium Home related services at your doorstep.</p>
                </div>
                <div class="col-md-6">
                    <div style="box-shadow:11px 11px 10px #ccc;" class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/MZHAl7aatE8" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="ourheading">
        <div class="container">
            <h1>OUR SERVICE</h1>
        </div>
    </section>
    <section id="ourservices">
        <div class="container">
            <div class="row">
                @foreach($categories as $category)
                <div class="col-md-3 col-sm-6">
                    <div class="servicebox1">
                        <a href="{{route('category-detail',['id'=>$category->id])}}"><img style="height:140px;" src="{{asset('assets/uploads/category-pics/'.$category->img)}}" alt="" ></a>
                        <h2><a href="#.">{{$category->title}}</a></h2>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center"><a href="#." class="btn btn-info">GET INSTANT ESTIMATION</a></div>
        </div>
    </section>
    <section class="whychoose">
        <div class="container">
            <h1>WHY CHOOSE US</h1>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/icon1.png')}}" alt="" >
                        <h2>High Quality <br>Advice</h2>
                        <p>Donec mollis lectus non
                            eros tempus lacinia.
                            Integer in facilisis t
                            urpsimply dumy is.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/icon2.png')}}" alt="" >
                        <h2>Professional <br>Labour</h2>
                        <p>Donec mollis lectus non
                            eros tempus lacinia.
                            Integer in facilisis t
                            urpsimply dumy is.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/icon3.png')}}" alt="" >
                        <h2>Quality <br>Work</h2>
                        <p>Donec mollis lectus non
                            eros tempus lacinia.
                            Integer in facilisis t
                            urpsimply dumy is.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/icon4.png')}}" alt="" >
                        <h2>After Sale <br>Service</h2>
                        <p>Donec mollis lectus non
                            eros tempus lacinia.
                            Integer in facilisis t
                            urpsimply dumy is.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="whychoose">
        <div class="container">
            <h1>WHO CHOOSE US</h1>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/pro1.png')}}" alt="" >
                        <h2>Lewis K. Gamino</h2>
                        <p>I have found TopRoffer to be
                            extremely professional and skilled.
                            Their service is first class.
                            I would not hesitate to use this
                            company again or recommend
                            them to any of my clients.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/pro1.png')}}" alt="" >
                        <h2>Lewis K. Gamino</h2>
                        <p>I have found TopRoffer to be
                            extremely professional and skilled.
                            Their service is first class.
                            I would not hesitate to use this
                            company again or recommend
                            them to any of my clients.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="whybox1">
                        <img src="{{asset('web-assets/images/pro1.png')}}" alt="" >
                        <h2>Lewis K. Gamino</h2>
                        <p>I have found TopRoffer to be
                            extremely professional and skilled.
                            Their service is first class.
                            I would not hesitate to use this
                            company again or recommend
                            them to any of my clients.</p>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="#." class="btn btn-info">VIEW OUR TESTIMONIALS</a>
            </div>
        </div>
    </section>

@endsection
