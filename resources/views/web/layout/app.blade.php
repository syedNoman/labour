<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="LMV">
    <title>Labour Hazir</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('web-assets/css/font-awesome.min.css')}}">
    <link href="{{asset('web-assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- End of head section HTML codes -->
    <link href="{{asset('web-assets/css/aos.css')}}" rel="stylesheet">
    <link href="{{asset('web-assets/css/main.css')}}" rel="stylesheet">
</head>
<body>

    <!-- Sidebar  -->
   @include('web.layout.header')

    <!-- Page Content  -->
    @yield('content')


    @include('web.layout.footer')


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="{{asset('web-assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('web-assets/js/aos.js')}}"></script>
    <script>
        AOS.init({
            easing: 'ease-in-out-sine'
        });
    </script>
</body>
</html>