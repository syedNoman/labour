<section id="callnow">
    <div class="container">
        <h1>Just Want To Talk To A Real Person?
            We're Here To Help!</h1>
       <h2><ul>
               <li>
                   Muhammad Ahsan
               </li>
               <li>
                   Head Operations
               </li>
               <li>+923024905581</li>
           </ul></h2>
        <a href="#." class="btn btn-info">Contact Us</a>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-9">
                <h1>SERVICES</h1>
               <div style="display: none">{{$categories=\App\Category::all()}}</div>
                @foreach($categories as $category)
                <p> <a href="#.">{{$category->title}}</a></p>
                @endforeach
                {{--<p> <a href="#.">Construction</a></p>--}}
                {{--<p> <a href="#.">Painting</a></p>--}}
                {{--<p> <a href="#.">Plumber</a></p>--}}
                {{--<p> <a href="#.">Loader</a></p>--}}
                {{--<p> <a href="#.">Mechanic</a></p>--}}
            </div>
            {{--<div class="col-md-3">--}}
                {{--<h1>About Us</h1>--}}
                {{--<p> <a href="#.">About us</a></p>--}}
                {{--<p> <a href="#.">YouTube Tutorials</a></p>--}}
                {{--<p> <a href="#.">Terms and Conditions</a></p>--}}
                {{--<p> <a href="#.">Contact Us</a></p>--}}
                {{--<p> <a href="#.">Privacy Policy</a></p>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h1>CONTACT US</h1>--}}
                {{--<p> <a href="#.">721 Lowes Alley Worthington,<br> OH 43085</a></p>--}}
                {{--<p> <a href="#.">Phone 800-123-4567</a></p>--}}
                {{--<p> <a href="#.">Email Address<br> contactus@toproofer.com</a></p>--}}
            {{--</div>--}}
            <div class="col-md-3">
                <h1>STAY CONNECTED</h1>
                <ul class="clearfix">
                    <li><a href="#."><img src="{{asset('web-assets/images/ul1.png')}}" alt=""></a></li>
                    <li><a href="#."><img src="{{asset('web-assets/images/ul2.png')}}" alt=""></a></li>
                    <li><a href="#."><img src="{{asset('web-assets/images/ul3.png')}}" alt=""></a></li>
                    <li><a href="#."><img src="{{asset('web-assets/images/ul4.png')}}" alt=""></a></li>
                    <li><a href="#."><img src="{{asset('web-assets/images/ul5.png')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<section id="smallfooter">
    <p>Copyright © All Rights Reserved 2020 </p>
</section>
<div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13596.378924661367!2d74.31768755!3d31.576449999999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1541238268782" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>