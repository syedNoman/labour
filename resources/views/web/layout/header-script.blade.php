<meta charset="utf-8">
<title>Wokiee - Responsive HTML5 Template</title>
<meta name="keywords" content="HTML5 Template">
<meta name="description" content="Wokiee - Responsive HTML5 Template">
<meta name="author" content="wokiee">
<link rel="shortcut icon" href="{{asset('favicon.ico')}}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="{{asset('css/theme.css')}}">