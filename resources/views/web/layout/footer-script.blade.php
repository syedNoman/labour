<script src="{{asset('external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('external/slick/slick.min.js')}}"></script>
<script src="{{asset('external/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('external/instafeed/instafeed.min.js')}}"></script>
<script src="{{asset('external/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('external/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('external/countdown/jquery.plugin.min.js')}}"></script>
<script src="{{asset('external/countdown/jquery.countdown.min.js')}}"></script>
<script src="{{asset('external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<!-- form validation and sending to mail -->
<script src="{{asset('external/form/jquery.form.js')}}"></script>
<script src="{{asset('external/form/jquery.validate.min.js')}}"></script>
<script src="{{asset('external/form/jquery.form-init.js')}}"></script>