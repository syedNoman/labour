@extends('web.layout.app')
@section('content')
    <div class="about">
        <div class="container">
            <h1>PROFILE PAGE</h1>
        </div>
    </div>
    <section id="profile">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="white" >
                        <div class="pic">
                            <img src="{{ asset('assets/uploads/labour-pics/'.$user->img) }}">
                            {{--<img src="{{asset('assets/uploads/category-pics/'.$category->img)}}">--}}
                            <h1>{{$user->first_name}} {{$user->last_name}}</h1>
                            <P>Experience<br>{{$user->experience}} Years</p>
                        </div>
                        <div class="colfoot">

                            <p>CONTACT US</br>0321-3213211</P>

                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class="">
                        {{--<img src="{{asset('assets/uploads/category-pics/'.$category->img)}}">--}}

                    </div>
                    <div class="discription">
                        <h1>Description</h1>
                        <p>{{$user->experience}}</p>
                        <h2>CNIC</h2>
                        <h2>{{$user->cnic}}</h2>
                        <h3>Location:<br>{{$user->address}} Pakistan.</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


