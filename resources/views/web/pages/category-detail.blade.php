@extends('web.layout.app')
@section('content')
    <div class="text-center">
        <img width="100%;" src="images/innerbaner.png" alt="">
    </div>
    <div id="innerwreper">
        <h1>{{$category->title}}</h1>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <ul>
                        @foreach($cats as $cat)
                        <li><a href="{{route('category-detail',['id'=>$cat->id])}}">{{$cat->title}}</a></li>
                            @endforeach
                    </ul>
                </div>
                <div class="col-md-8">
                    <h2>{{$category->title}} Services</h2>
                    <p>{!! $category->description !!} </p>
                    <img src="images/innerbaner1.png" style="width: 100%;" alt="">

                    <div class="plumb">
                        <h3>{{$category->title}} PROFILES</h3>
                    </div>



                    <div class="row row1">
                        @foreach($users as $user)
                        <div class="col-md-3">
                            <div style="margin-bottom: 20px;" class="back">
                                <img src="{{ asset('assets/uploads/labour-pics/'.$user->img) }}"
                                     width="60px" height="60px">
                                <h3>{{$user->first_name}} {{$user->last_name}}</h3>
                                <h3>Exp {{$user->experience}}</h3>
                                <button style="width: 100%;
    margin-top: 40px;" class="btn btn-light btn-sm"> <a  href="{{route('profile',['id'=>$user->id])}}">view profile</a></button>
                            </div>
                        </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection


