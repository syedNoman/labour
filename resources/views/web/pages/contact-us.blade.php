@extends('web.layout.app')
@section('content')
    <div class="text-center">
        <img width="100%;" src="images/save.png" alt="">
    </div>
    <div id="innerwreper">
        <h1>CONTACT US</h1>
    </div>
    <section id="contact">
        <div class="container">
            <p>For your convenience, you can request quotes by calling 123.456.7890 or through our online request form below and we will arrange to meet you at your location to provide you with a free, no obligation estimate.</p>
            <form id="con-form">
                <div class="form-row">
                    <div class="form-group  col-md-6">
                        <label id="finputname" for="inputname">First Name* </label>
                        <input type="text" name="inputname" class="form-control form-control-lg" id="inputname" placeholder=" first name"  minlength="2" required >
                    </div>
                    <div class="form-group col-md-6">
                        <label  id=" lname" for="linputname">Last Name* </label>
                        <input type="text" class="form-control form-control-lg" id="linputname" placeholder="last name" minlength="2" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label id="phone" for="inputphone">Phone* </label>
                        <input type="text" class="form-control form-control-lg" id="inputphone" placeholder="phone" minlength="7"  maxlength="12" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label id="Email4"  for="inputEmail4">Email*</label>
                        <input type="email" class="form-control form-control-lg" id="inputEmail4" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label id="address" for="inputaddress">Street Address* </label>
                        <input type="text" class="form-control form-control-lg" id="inputaddress" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label id="City" for="inputCity">City*</label>
                        <input type="text" class="form-control form-control-lg" id="inputCity" required>
                    </div>

                </div>
                <div class="form-group">
                    <div class="form-row align-items-center">
                        <div class="col-auto my-1 col-md-12">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Select Service *
                            </label>
                            <select class="custom-select custom-select-lg  mr-sm-2" id="inlineFormCustomSelect" required>
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="submit" class="btn btn-primary btn-lg pull-right" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection




