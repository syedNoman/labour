@extends('admin.layouts.app')
@section('content')
    @include("admin.layouts.lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div class="page-content">
                <div class="page-header">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h1>
                                Tables
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Scholar Tables
                                </small>
                            </h1>
                        </div>
                        <div class="col-sm-2">
                            <form action="/scholar" method="GET">
                                <div class="input-group">
                                    <input type="text" style=" width:200px; height: 33px;" class="form-control"
                                           name="searchTerm" placeholder="Search for..."
                                           value="{{ isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '' }}">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit" style="height: 33px;border: none;">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.page-header -->
                <br/>
                <div class="card-header">
                    <button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add
                        Scholar
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add
                                                            Scholar
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <form id="form" method="POST" action="/scholar"
                                                          enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Name
                                                                </lable>
                                                                <input class="form-control" id="nameid" name="name"
                                                                       type="text" placeholder="Enter Name"
                                                                       required>
                                                            </div>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    About
                                                                </lable>
                                                                <textarea name="about"
                                                                          id="summernote">{{ old('about') }}</textarea>
                                                            </div>


                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Add Scholar img</label>
                                                                <input class="form-control" id="id-input-file-2"
                                                                       name="picture_path"
                                                                       type="file">
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>Name</th>
                                        <th>About</th>
                                        <th>Picture</th>
                                        {{--<th>Role</th>--}}
                                        <th class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($scholars as $key => $scholar)

                                        <tr>
                                            <td>
                                                {{ ($scholars->currentpage() - 1) * $scholars->perpage() + 1 + $key }}

                                            </td>
                                            <td>{{ $scholar->name }}</td>
                                            <td>{!! $scholar->about !!}</td>
                                            <td><img src="{{ asset('uploads/scholars/'.$scholar->picture) }}"
                                                     width="50px" height="50px"></td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                            data-toggle="modal"
                                                            data-action="/scholar/{{ $scholar->id }}/edit"
                                                            data-target="#exampleModal2">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button title="Delete" type="button" data-toggle="modal"
                                                            data-target="#delete-modal"
                                                            data-action="/scholar/{{ $scholar->id }}"
                                                            class="show-delete-modal btn btn-xs btn-danger">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                <div class="text-right">{{ $scholars->links() }}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>

@endsection

