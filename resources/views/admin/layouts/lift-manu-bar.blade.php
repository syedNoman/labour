<ul class="nav nav-list">
    <li class="@if(request()->url() == route('admin.dashboard')){{'active'}}@endif">
        <a href="{{route('admin.dashboard')}}">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
    </li>

    <li class="@if(request()->url() == route('admin.category.index')){{'active open'}}
    @elseif(request()->url() == route('admin.category.create')){{'active open'}}
    @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-desktop"></i>
            <span class="menu-text">
								Category
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('admin.category.create')){{'active open'}}@endif">
                <a href="{{route('admin.category.create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add Category
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(request()->url() == route('admin.category.index')){{'active open'}}@endif">
                <a href="{{route('admin.category.index')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Category
                </a>

                <b class="arrow"></b>
            </li>

        </ul>
    </li>

    <li class="@if(request()->url() == route('admin.labour.index')){{'active open'}}
    @elseif(request()->url() == route('admin.labour.create')){{'active open'}}
    @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Labour </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url()==route('admin.labour.create')){{'active open'}}@endif">
                <a href="{{route('admin.labour.create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add Labour
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(request()->url()==route('admin.labour.index')){{'active open'}}@endif">
                <a href="{{route('admin.labour.index')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Labour
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <li class="@if(request()->url() == route('admin.sliders')){{'active open'}}@elseif(request()->url() == route('admin.slider-create')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Slider </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url()==route('admin.slider-create')){{'active open'}}@endif">
                <a href="{{route('admin.slider-create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add Slider
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(request()->url()==route('admin.sliders')){{'active open'}}@endif">
                <a href="{{route('admin.sliders')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Slider
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    <li class="@if(request()->url() == route('admin.projects')){{'active open'}}@elseif(request()->url() == route('admin.project-create')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Projects </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url()==route('admin.project-create')){{'active open'}}@endif">
                <a href="{{route('admin.project-create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add Projects
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(request()->url()==route('admin.projects')){{'active open'}}@endif">
                <a href="{{route('admin.projects')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Projects
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>


</ul><!-- /.nav-list -->