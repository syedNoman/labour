@extends('admin.layouts.form-app')
@section('content')
    @include("admin.layouts.form-lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div style="background-color:#CCCCCC" class="page-content">
                {{--<div class="page-header">--}}
                   {{----}}
                {{--</div><!-- /.page-header -->--}}

                <div class="card-header">
                    {{--<div>--}}
                    {{--vds--}}
                    {{--</div>--}}
                    {{--<button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add--}}
                    {{--cources PDF--}}
                    {{--</button>--}}
                </div>
                <div style="margin-top: 34px;" class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div style="    color: black;
                            font-size: 14px;
                            font-family: serif;" class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add
                                                            cources
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </h5>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                @include('web.pages.contact-us2')

                                </table>
                                {{--@if()--}}

                                {{--@endif--}}
                                {{--<div class="text-right">{{ $users->links()}}</div>--}}
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });
        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

