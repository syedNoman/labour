@extends('admin.layouts.app')
@section('content')
    @include("admin.layouts.lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div class="page-content">
                <div class="page-header">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h1>
                                Tables
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Cource PDF Table
                                </small>
                            </h1>
                        </div>
                        <div class="col-sm-2">
                            <form action="/search" method="post">
                                {{csrf_field()}}
                                <div class="input-group">
                                    <input type="text" style=" width:200px; height: 33px;" class="form-control"
                                           name="search" placeholder="Search for..."
                                           value="{{ old('search') }}">

                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit" style="height: 33px;border: none;">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.page-header -->
                <br/>
                <div class="card-header">
                    {{--<div>--}}
                        {{--vds--}}
                    {{--</div>--}}
                    {{--<button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add--}}
                        {{--cources PDF--}}
                    {{--</button>--}}
                </div>
                <div style="margin-top: 34px;" class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Family name</th>
                                        <th>Email</th>
                                        <th>PDF View/Download</th>
                                        <th class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key => $user)
                                        <tr>
                                            <td>
                                                {{ ($users->currentpage() - 1) * $users->perpage() + 1 + $key }}
                                            </td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->middle_name }}</td>
                                            <td>{{ $user->family_name }}</td>
                                            <td>{{ $user->email }}</td>

                                            <td><a title="view" class="btn btn-xs btn-primary"
                                                   href="{{ route('view-form',['id' => $user->id]) }}">
                                                    <i class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>

                                                {{--<a href="{{ route('delete-slider', ['id' => $slider->id]) }}"> <i style="color: red"--}}
                                                                                                                  {{--class="fa fa-trash"></i></a>--}}
                                            </td>

                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                            data-toggle="modal"
                                                            data-action="/cources/{{ $user->id }}/edit"
                                                            data-target="#exampleModal2">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button title="Delete" type="button" data-toggle="modal"
                                                            data-target="#delete-modal"
                                                            data-action="/cources/{{ $user->id }}"
                                                            class="show-delete-modal btn btn-xs btn-danger">
                                                         <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                {{--@if()--}}

                                    {{--@endif--}}
                                <div class="text-right">{{ $users->links()}}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });
        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

