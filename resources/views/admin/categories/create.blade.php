@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="{{route('admin.dashboard')}}">Dashboard</a>
                    </li>
                    <li class="active">Category</li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        Category
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Add Category
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->

                        <div class="row">
                            <div class="col-lg-12">
                                @if(Session::has('message'))
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                @endif
                                    <form method="POST" enctype="multipart/form-data" action="@if(isset($category)) {{ route('admin.category.update',$category->id)}} @else{{ route('admin.category.store')}} @endif" >


                                <div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-xs-12" class="card-body">
                                    <div class="row">
                                    <div  class="col-lg-9">
                                        @csrf
                                        @if(isset($category))
                                            @method('PUT')
                                        @endif
                                        <input type="hidden" value="{{@$category->slug}}" id="slug_title"  name="slug">
                                        <div style="margin-top:20px;" class="form-group row">
                                            <label for="email" class="col-xs-12 col-form-label text-md-right ">{{ __('Title:') }}</label>
                                            <div class="col-xs-12">
                                                <input id="title" type="text" class="slugify form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{@$category->title}}">
                                                {{--<span style="color:#7c7c7c">{{'Link: https://myshop/'}}</span><span id="slug-text" style="color:#7c7c7c">{{('')}}</span>--}}
                                                @if ($errors->has('title'))
                                                    <span class="invalid-feedback">
                                                    <strong style="color: red;">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Description:') }}</label>
                                            <div class="col-xs-12">
                                                <textarea id=""  class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{!! @$category->description !!}</textarea>
                                                @if ($errors->has('description'))
                                                    <span class="invalid-feedback">
                                                    <strong style="color: red;">{{ $errors->first('description') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            @php(
                                            $ids=(isset($category->childrens) && $category->childrens->count()>0)?
                                            array_pluck($category->childrens,'id'):null
                                            )

                                            <label for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Select Category:') }}</label>
                                            <div class="col-xs-12">
                                                <select data-placeholder="Choose a Prent Category"  class="select form-group col-xs-12" multiple name="parent_id[]">
                                                    @foreach($categories as $cat)
                                                        <option value="{{$cat->id}}" @if(!is_null($ids)&& in_array($cat->id,$ids)) selected @endif>{{$cat->title}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-lg-3">
                                            <ul class="list-group row">
                                                <li class="list-group-item active"><h5>Category Image</h5></li>
                                                <li class="list-group-item">
                                                    <div class="mb-3">
                                                        <div class="custom-file ">
                                                            <input    type="file" multiple  class="form-control{{ $errors->has('thumbnail') ? ' is-invalid' : '' }} custom-file-input" value="{{isset($cat->thumbnail)}}" name="thumbnail" id="id-input-file-2">
                                                            @if ($errors->has('thumbnail'))
                                                                <span class="invalid-feedback">
                                                                    <strong style="color: red;">{{ $errors->first('thumbnail') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="img-thumbnail text-center">
                                                        <img src="@if(isset($category)) {{asset('assets/uploads/category-pics/'.$category->img)}} @else {{asset('assets/images/no-thumbnail.jpeg')}} @endif" id="imgthumbnail" class="img-thumbnail" alt="">
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-12  pull-right">
                                            @if(isset($category))
                                                <input type="submit" class="btn btn-primary" value="Update Category">
                                            @else
                                                <input type="submit" class="btn btn-primary" value="Add Category">
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                    </form>
                            </div><!-- /.span -->

                        </div><!-- /.row -->

                        <!-- /.row -->

                        <div class="hr hr-18 dotted hr-double"></div>

                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

            </div><!-- /.page-content -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#summernote').summernote({
            height: 100
        });


        //        $(".select").chosen({max_selected_options: 5});
        $(".select").chosen({width: "100%"});

        $('.slugify').keyup(function () {
            var url=($(this)).val();
//          str = url.split(" ").join("-").toLowerCase();
            str = url.replace(/\s+/g, '-').toLowerCase();
            $('#slug-text').text(str);
            $('#slug_title').val(str);
        });

        //        $('#title').on('keyup',(function () {
        //            $('#slug-text').text('ok');
        //        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });

        $('#id-input-file-2').on('change', function() {
            var file = $(this).get(0).files;
            var reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.addEventListener("load", function(e) {
                var image = e.target.result;
                $("#imgthumbnail").attr('src', image);
            });
        });



    </script>

@endsection