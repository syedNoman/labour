@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="{{route('admin.dashboard')}}">Dashboard</a>
                    </li>
                    <li class="active">Labour</li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        Labour
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Add Labour
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                @if(Session::has('message'))
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                @endif
                                <div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-xs-12" class="card-body">

                                    <form  action="{{route('admin.add-slider')}}"  method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <div class="col-lg-9">
                                                <div class="form-group row">
                                                    <label style="margin-top:20px;" for="email" class="col-xs-12 col-form-label text-md-right">{{ __('First Heading') }}</label>
                                                    <div class="col-xs-12">
                                                        <input id="first_heading" type="text" class="form-control{{ $errors->has('first_heading') ? ' is-invalid' : '' }}" name="first_heading"  required>
                                                        @if ($errors->has('first_heading'))
                                                            <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('first_heading') }}</strong>
                                    						</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Last Name') }}</label>
                                                    <div class="col-xs-12">
                                                        <input id="second_heading" type="text" class="form-control{{ $errors->has('second_heading') ? ' is-invalid' : '' }}" name="second_heading"  required>
                                                        @if ($errors->has('second_heading'))
                                                            <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('second_heading') }}</strong>
                                    						</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Description') }}</label>
                                                    <div class="col-xs-12">
                                                        <textarea rows="5" id="desc" type="number" class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}" name="desc" ></textarea>
                                                        @if ($errors->has('desc'))
                                                            <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('desc') }}</strong>
                                    						</span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">
                                                <ul class="list-group row">
                                                    <li class="list-group-item active"><h5>Slider Image</h5></li>
                                                    <li class="list-group-item">
                                                        <div class="mb-3">
                                                            <div class="custom-file ">
                                                                {{--<input type="file"  class="custom-file-input" name="thumbnail" id="id-input-file-2">--}}
                                                                <input aria-required="true" type="file" multiple  class="custom-file-input" name="thumbnail" id="id-input-file-2" required>
                                                                {{--<label class="custom-file-label" for="thumbnail">Choose file</label>--}}
                                                            </div>
                                                        </div>
                                                        <div class="img-thumbnail text-center">
                                                            <img src="@if(isset($user)) {{asset('assets/uploads/labour-pics/'.$user->img)}} @else {{asset('assets/images/no-thumbnail.jpeg')}} @endif" id="imgthumbnail" class="img-thumbnail" alt="">
                                                        </div>
                                                    </li>
                                                </ul>

                                                <div class="form-group row mb-0">
                                                    <div class="col-xs-12">
                                                        <input type="submit" class="btn btn-primary" value="Save Slider">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <div class="hr hr-18 dotted hr-double"></div>

                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#summernote').summernote({
            height: 100
        });


        //        $(".select").chosen({max_selected_options: 5});
        $(".select").chosen({width: "100%"});

        $('.slugify').keyup(function () {
            var url=($(this)).val();
//          str = url.split(" ").join("-").toLowerCase();
            str = url.replace(/\s+/g, '-').toLowerCase();
            $('#slug-text').text(str);
            $('#slug_title').val(str);
        });

        //        $('#title').on('keyup',(function () {
        //            $('#slug-text').text('ok');
        //        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });

        $('#id-input-file-2').on('change', function() {
            var file = $(this).get(0).files;
            var reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.addEventListener("load", function(e) {
                var image = e.target.result;
                $("#imgthumbnail").attr('src', image);
            });
        });



    </script>

@endsection