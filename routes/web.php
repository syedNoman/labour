<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/migrate', function () {
    $clearcache = Artisan::call('migrate');
    echo "migrate run<br>";

    $clearview = Artisan::call('view:clear');
    echo "View cleared<br>";

    $clearconfig = Artisan::call('config:cache');
    echo "Config cleared<br>";

    $cleardebugbar = Artisan::call('debugbar:clear');
    echo "Debug Bar cleared<br>";
});

Route::get('/logout', 'HomeController@logout')->name('logout-dash');
Route::get('/login', 'HomeController@index')->name('home');
Route::get('/', 'WebController@index');
Route::get('/projects-show', 'WebController@projectsShow')->name('projects-show');
Route::get('/profile', 'WebController@profile');
Route::get('/category/{id}', 'WebController@categoryDeatil')->name('category-detail');
Route::get('/profile/{id}', 'WebController@profile')->name('profile');

Route::get('/about-us', 'WebController@AboutUs')->name('about-us');
Route::get('/contact-us', 'WebController@ContactUs')->name('about-us');
Auth::routes();



Route::group(['as'=>'admin.','middleware'=>['auth','admin'],'prefix'=>'admin'],function (){

    Route::get('/sliders', 'AdminController@Slider')->name('sliders');
    Route::get('/sliders-create', 'AdminController@SliderCreate')->name('slider-create');
    Route::post('/create-sliders', 'AdminController@CreateSlider')->name('add-slider');
    Route::get('/delete-sliders/{id}', 'AdminController@DelSlider')->name('delete-slider');
    Route::get('/edit-sliders/{id}', 'AdminController@EditSlider')->name('edit-slider');
    Route::post('/edit-sliders/{id}', 'AdminController@UpdateSlider')->name('update-slider');


    Route::get('/projects', 'ProjctController@Project')->name('projects');
    Route::get('/projects-create', 'ProjctController@ProjectCreate')->name('project-create');
    Route::post('/create-projects', 'ProjctController@CreateProject')->name('add-project');
    Route::get('/delete-projects/{id}', 'ProjctController@DelProject')->name('delete-project');
    Route::get('/edit-projects/{id}', 'ProjctController@EditProject')->name('edit-project');
    Route::post('/edit-projects/{id}', 'ProjctController@UpdateProject')->name('update-project');



    Route::get('/dashboard','AdminController@dashboard')->name('dashboard');
    Route::get('category/delete/{id}','DeleteController@delCategory')->name('delete-category');
    Route::get('/labour','LabourController@createLabour')->name('labourcreate');
    Route::get('labour/delete/{id}','DeleteController@delLabour')->name('delete-labour');


    Route::post('labour/{id}','LabourController@uppdateLabour')->name('updateLabour');
    Route::resource('labour','LabourController');
    Route::get('/edit-labour/{id}','AdminController@editlabour')->name('editLabour');

    Route::resource('category','CategoryController');





});
