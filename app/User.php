<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use App\Category;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $table = 'category_users';

    protected $fillable = [
        'role_id',
        'cat_id',
        'first_name',
        'last_name',
        'img',
        'phone',
        'cnic',
        'experience',
        'address',
        'email',
        'password',

        ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function categories(){
//        return $this->belongsToMany('App\Category');

        return $this->belongsToMany('App\Category','category_user','category_id','id');
    }



}
