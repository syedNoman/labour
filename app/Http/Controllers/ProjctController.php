<?php

namespace App\Http\Controllers;

use App\Projct;
use Illuminate\Http\Request;

class ProjctController extends Controller
{
    public function Project()
    {
//        dd('oj');
        $projects=Projct::all();
//        dd($sliders);
        return view('admin.project.project',compact('projects'));
    }

    public function ProjectCreate()
    {
        $projects=Projct::all();
        return view('admin.project.create',compact('projects'));
    }

    public function CreateProject(Request $request)
    {



        if($request->file('thumbnail')) {

            $file = $request->file('thumbnail');
            $destinationPath = 'assets/uploads/project-pics';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['img' => $filename]);

        }



        $create=Projct::create($request->all());
        if($create){
            return  back()->with('message','Project Added Successfully');
        }
    }

    public function DelProject($id){
//        $deluser=Slider::find($id)->delete();
        $del = Projct::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message', 'Delete Project Successfully');
        }
    }

    public function EditProject($id){
//        dd($id);
        $project=Projct::find($id);
        return view('admin.project.editsproject',compact('project'));
    }

    public function UpdateProject($id,Request $request){
        $update_silder=Projct::find($id);

        if ($file = $request->file('thumbnail')) {
            $destinationPath = 'assets/uploads/project-pics';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['img' => $filename]);
            @unlink(('assets/uploads/slider-pics/' . $update_silder->img));
        }

        $update=$update_silder->update($request->all());

        if ($update) {
            return back()->with('message', 'Project Update Successfully');
        }


    }
}
