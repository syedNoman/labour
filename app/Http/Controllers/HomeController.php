<?php

namespace App\Http\Controllers;

use App\Mail\RegisterUser;
use App\User;
//use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Mail;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.welcome');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }

    public function send()
    {
        Mail::to('nomannafees043@gmail.com')->send(new RegisterUser('nomannafees043@gmail.com','this is body'));
        return response()->json(['message' => 'Request completed']);
    }


    public function getuser(){
        $users=User::paginate(5);
        return view('admin.users.users',compact('users'));
    }

    public function search(Request $request){
    User::where('role', '=', 2)->first();
        if ($request->has('search')){
            $search = $request->get('search');
            $users = User::where('first_name', 'like', '%' . $search . '%')
                            ->orWhere('middle_name', 'like', '%' . $search . '%')
                            ->orWhere('family_name', 'like', '%' . $search . '%')
                            ->orWhere('email', 'like', '%' . $search . '%')
                            ->orWhere('gender', 'like', '%' . $search . '%')
                            ->orWhere('date_of_birth', 'like', '%' . $search . '%')
                            ->orWhere('address', 'like', '%' . $search . '%')
                            ->orWhere('city', 'like', '%' . $search . '%')
                            ->orWhere('postcode', 'like', '%' . $search . '%')
                            ->orWhere('county', 'like', '%' . $search . '%')
                            ->orWhere('country', 'like', '%' . $search . '%')
                            ->orWhere('area_code', 'like', '%' . $search . '%')
                            ->orWhere('number', 'like', '%' . $search . '%')
                            ->orWhere('country_code', 'like', '%' . $search . '%')
                            ->where('role', '=', 2)->paginate(3);
        }
        return view('admin.users.users',compact('users'));
    }


    public function formGet(){
//        dd('ok');
        return view('admin.contact-form.view-from');
    }

    public function viewForm(Request $request,$id){

//        dd($id);
        $user=User::where('id',$id)->first();
//        $employ = DB::select("SELECT * FROM employment_detail WHERE user_id = '$id' ");
        $employ = DB::table('employment_detail')->where('user_id',$id)->first();
        $education = DB::table('education')->where('user_id',$id)->first();
        $opq = DB::table('other_professional_qualifications')->where('user_id',$id)->first();
        $referees = DB::table('referees')->where('user_id',$id)->first();
        return view('admin.contact-form.view-from',compact('user','employ','education','opq','referees'));
    }

    public function pdf(){
        $data=['nomi'];

        $pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');
       // return $pdf->stream('invoice10.pdf');
//        return $pdf->download('invoice10.pdf');
//        PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf');
        return PDF::loadFile('assets/pdf/invoice.html')->setPaper('a3.5', 'landscape')->setWarnings(false)->save('assets/pdf/invoice.pdf')->stream('download.pdf');



//        set_time_limit(300);
//        $pdf = PDF::loadView('invoice', $data);
//        return $pdf->download('invoice10.pdf');
    }

}
