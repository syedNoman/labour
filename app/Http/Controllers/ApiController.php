<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getCategory(){
        $category=Category::all();
        return $category;
    }

    public function getLabour(Request $request){
//        dd($request->id);
        $user=User::where('cat_id',$request->id)->first();
        return $user;
    }

    public function getProfile($id){
        $user=User::where('cat_id',$id)->first();
        return $user;
    }


}
