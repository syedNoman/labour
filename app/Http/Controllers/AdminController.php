<?php

namespace App\Http\Controllers;

use App\Category;
use App\Slider;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    public function dashboard()
    {
        return view('admin.welcome');
    }

    public function editlabour(Request $request)
    {
//            dd('pl');
//            dd($request->id);
        $user=User::find($request->id);
//        dd($user);
        $categories=Category::all();
        return view('admin.labours.edit',compact('categories','user'));
    }

    public function Slider()
    {
//        dd('oj');
        $sliders=Slider::all();
//        dd($sliders);
        return view('admin.slider.sliders',compact('sliders'));
    }

    public function SliderCreate()
            {
                $sliders=Slider::all();
                return view('admin.slider.create',compact('sliders'));
            }

    public function CreateSlider(Request $request)
    {

//        dd($request->all());

        if($request->file('thumbnail')) {

            $file = $request->file('thumbnail');
            $destinationPath = 'assets/uploads/slider-pics';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['img' => $filename]);

        }



        $create=Slider::create($request->all());
        if($create){
            return  back()->with('message','Slider Added Successfully');
        }
    }

    public function DelSlider($id){
//        $deluser=Slider::find($id)->delete();
        $del = Slider::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message', 'Delete Category Successfully');
        }
    }

    public function EditSlider($id){
//        dd($id);
        $slider=Slider::find($id);
        return view('admin.slider.editslider',compact('slider'));
    }

    public function UpdateSlider($id,Request $request){
        $update_silder=Slider::find($id);

        if ($file = $request->file('thumbnail')) {
            $destinationPath = 'assets/uploads/slider-pics';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['img' => $filename]);
            @unlink(('assets/uploads/slider-pics/' . $update_silder->img));
        }

        $update=$update_silder->update($request->all());

            if ($update) {
                return back()->with('message', 'Slider Update Successfully');
            }


    }
}
