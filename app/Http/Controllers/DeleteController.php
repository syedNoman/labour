<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function delCategory($id){

        $category = Category::findOrFail($id);
        $delt=$category->delete();
        if($delt){
            return back()->with('message', 'Delete Category Successfully');
        }
    }

    public function delLabour($id){

        $user = User::findOrFail($id);
        $delt=$user->delete();
        if($delt){
            return back()->with('message', 'Delete Category Successfully');
        }
    }
}
