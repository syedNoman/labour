<?php

namespace App\Http\Controllers;

use App\Category;
use App\Labour;
use App\User;
use Illuminate\Http\Request;

class LabourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where('role_id',0)->paginate(20);
        return view('admin.labours.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        dd('ok');
        $categories = Category::with('childrens')->get();
        return view('admin.labours.create', compact('categories'));
    }

    public function createLabour()
    {
        dd('ok');
        $categories = Category::with('childrens')->get();
        return view('admin.labours.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $file = $request->file('thumbnail');
        $destinationPath = 'assets/uploads/labour-pics';
        $filename = $file->getClientOriginalName();
        $filename = time() . $filename;
        $file->move($destinationPath, $filename);
        $request->merge(['img' => $filename]);

//        dd($request->all());
        $user=User::create($request->all());

        if($user){
            return  back()->with('message','Labour Added Successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Labour  $labour
     * @return \Illuminate\Http\Response
     */
    public function show(Labour $labour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Labour  $labour
     * @return \Illuminate\Http\Response
     */
//    public function edit(Labour $labour,$id)
//    {
////            dd('pl');
//        $user=User::find($id);
////        dd($user);
//        $categories=Category::all();
//        return view('admin.labours.edit',compact('categories','user'));
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labour  $labour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Labour $labour)
    {
        dd('update');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Labour  $labour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Labour $labour)
    {
        //
    }

    public function uppdateLabour(Request $request,$id){

        $user=User::find($id);

        if ($file = $request->file('thumbnail')) {
            $destinationPath = 'assets/uploads/labour-pics';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['img' => $filename]);

            @unlink(public_path('uploads/scholars/' . $user->picture));
        }

        $user->update($request->all());
        return  back()->with('message','user update Successfully');
    }
}
