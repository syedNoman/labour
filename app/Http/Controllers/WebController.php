<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\UserRegistr;
use App\Projct;
use App\Slider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mail;

class WebController extends Controller
{

    public function send()
    {

        $title = 'title';
        $content = 'content';

        Mail::to('nomannafees043@gmail.com')->send(new UserRegistr('','this is body'));

        return response()->json(['message' => 'Request completed']);
    }

    public function index(){
        $categories=Category::all();
        $slider=Slider::all();
        return view('web.index',compact('categories','slider'));
    }

    public function AboutUs(){
        return view('web.pages.about');
    }

    public function ContactUs(){
        return view('web.pages.contact-us');
    }

    public function projectsShow(){
        $projects=Projct::all();
        return view('web.pages.project',compact('projects'));
    }



    public function categoryDeatil($id){

             $category=Category::where('id',$id)->first();
             $cats=Category::all();
             $users=User::where('cat_id',$id)->get();
//             dd($users);
             return view('web.pages.category-detail',compact('category','cats','users'));
    }

public function profile($id){
        $user=User::where('id',$id)->first();
        $category=Category::where('id',$user->cat_id)->first();
        return view('web.pages.profile',compact('user','category'));
}


}
