<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScholarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('searchTerm')) {
            $searchTerm = $request->get('searchTerm');
            $scholars = User::where('name', 'like', '%' . $searchTerm . '%')->where('role', '=', 2)->paginate(20);
        } else {
            $scholars = User::latest()->where('role', '=', 2)->paginate(20);
        }
        return view('admin.scholars.list-scholar', compact('scholars'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $file = $request->file('picture_path');
        $destinationPath = public_path() . '/uploads/scholars';
        $filename = $file->getClientOriginalName();
        $filename = time() . $filename;
        $file->move($destinationPath, $filename);


        $request->merge(['picture' => $filename]);
        $request->request->add(['role' => 2]);
        $create = User::create($request->all());

        if ($create) {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $scholar = User::findOrFail($id);
        return view('admin/scholars/edit-scholar', compact('scholar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $scho = User::find($id);

        if ($file = $request->file('picture_path')) {
            $destinationPath = public_path() . '/uploads/scholars';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);

            $request->merge(['picture' => $filename]);
            @unlink(public_path('uploads/scholars/' . $scho->picture));
        }

        $scho->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = User::findOrFail($id);
        $product->delete();

        return back();
    }
}
