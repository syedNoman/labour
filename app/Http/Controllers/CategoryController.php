<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::paginate(7);
        return view('admin.categories.index',compact('categories'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.categories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//       dd($request->all());
        $request->validate([
            'title'=>'required|min:3',
            'description'=>'required|min:3',
            'thumbnail'=>'required',

        ]);
//        dd($request->all());

        $file = $request->file('thumbnail');
        $destinationPath = 'assets/uploads/category-pics';
        $filename = $file->getClientOriginalName();
        $filename = time() . $filename;
        $file->move($destinationPath, $filename);
        $request->merge(['img' => $filename]);

        $category=Category::create($request->only('title','description','slug','img'));
        $category->childrens()->attach($request->parent_id);
        if($category){
            return  back()->with('message','Category Added Successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories=Category::all();
        return view('admin.categories.create',['categories'=>$categories,'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
//            $category->title = $request->title;
//            $category->description = $request->title;
//            $category->slug = $request->slug;
//            $category->img = $upimg;
////        $category->slug=$request->slug;
//            //detach all parent Category
//            $category->childrens()->detach();
//            //attach selected parent Category
//            $category->childrens()->attach($request->parent_id);
//            //save  recode to the database
//            $saved = $category->save();
//        }

            $scho = Category::find($category->id);

            if ($file = $request->file('thumbnail')) {
                $destinationPath = 'assets/uploads/category-pics';
                $filename = $file->getClientOriginalName();
                $filename = time() . $filename;
                $file->move($destinationPath, $filename);
                $request->merge(['img' => $filename]);
                @unlink(('assets/uploads/category-pics/' . $scho->img));
            }
                    // detach all parent Category
                     $category->childrens()->detach();
                    //attach selected parent Category
                    $category->childrens()->attach($request->parent_id);
                     $update=$scho->update($request->only('title','description','slug','img'));
                     if($update){
                         return back()->with('message', 'Recode Update Successfully');
                     }

//            return back();





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
