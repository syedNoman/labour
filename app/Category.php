<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;
class Category extends Model
{


    protected $guarded=[];


    public function products(){
        return $this->belongsToMany('App\Product');
//        return $this->belongsToMany('App\Category','product_category','category_id','id');
    }

    public function users(){
        return $this->belongsToMany('App\User');
//        return $this->belongsToMany('App\Category','product_category','category_id','id');
    }

    public function childrens(){
    return $this->belongsToMany(Category::class,'category_parent','category_id','parent_id');
    }
}
