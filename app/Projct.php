<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projct extends Model
{
    protected $fillable=[
        'title',
        'desc',
        'img',
    ];
}
