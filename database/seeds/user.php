<?php

use App\Profile;
use App\Role;
use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
       DB::table('users')->insert([
           'email'=>'admin@admin.com',
           'password'=>bcrypt('admin123456'),
           'role_id'=>1,
       ]);

    }
}
